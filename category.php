<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */

get_header();
?>
<div class="category-page">
    <h1><?php _e('Categories for ','plainwp');single_cat_title();?></h1>
    <?php get_template_part('loop');?>
    
    <div class="pagination">
        <?php plainwp_pagination();?>
    </div>
</div>
<?php
get_sidebar();
get_footer();
?>