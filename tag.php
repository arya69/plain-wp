<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
  *  ============================ */
get_header();
?>
<div class="tag-page">
    <h1>
        <?php _e('Tag Archive: ','plainwp'); echo single_tag_title('', false);?>
    </h1>
    <?php get_template_part('loop');?>
    <div class="pagination">
        <?php plainwp_pagination();?>
    </div>
</div>
<?php
get_sidebar();
get_footer();
?>