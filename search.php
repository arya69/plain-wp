<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
  *  ============================ */
get_header();
?>
<div class="search-page">
    <h1><?php echo sprintf(__('%s Search Results for ','plainwp'), $wp_query->found_posts);echo get_search_query();?></h1>
    <?php get_template_part('loop');?>
    <div class="pagination">
        <?php plainwp_pagination();?>
    </div>
</div>
<?php
get_sidebar();
get_footer();
?>