<?php load_theme_textdomain('plainwp'); ?>
<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */

require_once 'admin/wp-hack.php';
require_once 'admin/register_component.php';
//require_once 'admin/wp-admintext.php';

function plainwp_scripts(){
    if (!is_admin()){
        wp_deregister_script('jquery');
        
        wp_register_script('jquery', get_template_directory_uri().'/js/jquery.latest.min.js', false, '1.10.1');
        wp_enqueue_script('jquery');
        
        wp_register_script('modern', get_template_directory_uri().'/js/modernizr-2.6.2.min.js', array(), '2.6.2', false);
        wp_enqueue_script('modern');
        
        wp_register_script('app', get_template_directory_uri().'/js/app.js', array(),'1.0.1');
        wp_enqueue_script('app');
    }
}add_action('init','plainwp_scripts');

function plainwp_styles(){
    if (!is_admin()){
        wp_register_style('opensans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300', array(),'1.0.1');
        wp_enqueue_style('opensans');

        wp_register_style('web', get_template_directory_uri().'/css/app.css', array(),'1.0.1');
        wp_enqueue_style('web');
    }
}add_action('wp_enqueue_scripts','plainwp_styles');

//add theme support
if (function_exists('add_theme_support')){
    add_theme_support('menus');
    add_theme_support('post-thumbnails');
    add_theme_support('custom-background');
    add_theme_support('post-formats');
//    add_image_size('large',700,150,true);
//    add_image_size('medium',400,200,true);
//    add_image_size('small',200,100,true);
//    add_image_size('post-thumbnail',150,100,true);    
}

//unset default post thumbnail size to save server space
function remove_default_image_sizes($sizes){
    unset($sizes['thumbnail']);
    unset($sizes['medium']);
    unset($sizes['large']);
    return $sizes;
}add_filter('intermediate_image_sizes_advanced','remove_default_image_sizes');

//pagination
function plainwp_pagination(){
    global $wp_query;
    $var = 999999999;
    echo paginate_links(array(
        'base'      => str_replace($var, '%#%', get_pagenum_link($var)),
        'format'    => '?paged=%#%',
        'current'   => max(1, get_query_var('paged')),
        'total'     => $wp_query->max_num_pages
    ));
}

// Fragment chache
function fragment_cache($key, $ttl, $function) {
  if ( is_user_logged_in() ) {
    call_user_func($function);
    return;
  }
  $key = apply_filters('fragment_cache_prefix','fragment_cache_').$key;
  $output = get_transient($key);
  if ( empty($output) ) {
    ob_start();
    call_user_func($function);
    $output = ob_get_clean();
    set_transient($key, $output, $ttl);
  }
  echo $output;
}
?>