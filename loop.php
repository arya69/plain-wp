<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */
fragment_cache('blog_loop', DAY_IN_SECONDS, function(){
    if (have_posts()):while (have_posts()):the_post();?>
    <!--post div-->
    <div id="post-<?php the_ID();?>" class="post">
        <!--thumb-->
        <?php if (has_post_thumbnail()){ the_post_thumbnail();}?>
        <!--thumb-->
        <!--title-->
        <h2>
            <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                <?php the_title();?>
            </a>
        </h2>
        <!--title-->
        <!--    meta description-->
        <span class="meta">
            <?php _e('Published by ', 'plainwp');?><?php the_author_posts_link();?><?php the_time('F j, Y');?><?php the_time('g:i a');?>
        </span>
        <!--    meta description-->
        <div class="excerpt">
            <?php the_excerpt();?>
        </div>
        <?php edit_post_link();?>
    </div>
    <!--post div-->
    <?php endwhile;
    else:?>
    <div class="no-post">
        <h2><?php _e('Sorry, no post here.', 'plainwp');?></h2>
    </div>
    <?php endif;
    });
?>