<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */
?>
<form method="get" class="search" action="<?php echo home_url(); ?>" >
    <input id="s" type="text" name="s" placeholder="<?php _e('Search','plainwp');?>">
    <input class="searchsubmit button" type="submit" value="<?php _e( 'Search', 'plainwp' ); ?>">
</form>