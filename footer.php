    
        <footer>
            <p class="copyright">
                &COPY; <?php echo date('Y');?> by <?php bloginfo('name');?><br/>
                Powered by <a href="//wordpress.org" target="_blank" title="Wordpress">Wordpress</a>
            </p>
        </footer>
        </div>
        <!--page wrap-->
        <?php wp_footer();?>
    </body>
</html>