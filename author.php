<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */

get_header();
?>
<div class="author-page">
    <?php if(have_posts()):the_post();?>
        <h1><?php _e('Author Archives for ','plainwp'); echo get_the_author();?></h1>
        <?php if (get_the_author_meta('description')):?>
            <?php echo get_avatar(get_the_author_meta('user_email'));?>
            <h2><?php _e('About','plainwp'); echo get_the_author();?></h2>
            <?php the_author_meta('description');?>
        <?php endif;?>
        <?php rewind_posts();while (have_posts()):the_post();?>
            <div id="post-<?php the_ID();?>" class="post">
                <!--thumb-->
                <?php if(has_post_thumbnail()){
                    $src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>
                    <a href="<?php the_permalink();?>" title="<?php echo get_the_author_meta('nickname').'-'.  get_the_title(); ?>">
                        <img src="<?php echo $src;?>" alt="<?php echo get_the_author_meta('nickname').'-'.  get_the_title(); ?>"/>
                    </a>
                <?php }?>
                <!--thumb-->
                <!--title-->
                <h2>
                    <a href="<?php the_permalink();?>" title="<?php echo get_the_author_meta('nickname').'-'.  get_the_title(); ?>"><?php the_title();?></a>
                </h2>
                <!--title-->
                <div class="meta">
                    <span class="tag"><?php the_tags(__('Tags: ','plainwp'),', ','');?></span><br/>
                    <span class="categories"><?php _e( 'Categorised in: ', 'plainwp' ); the_category(', ');?></span>
                </div>
                <div class="excerpt">
                    <?php the_excerpt();?>
                </div>
                <?php edit_post_link();?>
            </div>
        <?php endwhile;?>
    <?php else:?>
            <div class="no-post">
                <h2><?php _e('Sorry, no post here.', 'plainwp');?></h2>
            </div>
    <?php endif;?>
    <div class="pagination">
        <?php plainwp_pagination();?>
    </div>
</div>
<?php
get_sidebar();
get_footer();
?>