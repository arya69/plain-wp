<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset');?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>
            <?php 
            wp_title('');
            if (wp_title('', false)){
                echo ' :';
            }
            bloginfo('name');
            ?>
        </title>
        <meta name="description" content="<?php bloginfo('description');?>">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut_icon" href=""/>
        
        <?php wp_head();?>
        
    </head>
    <body>
        <!--page wrap-->
        <div class="wrap">
            <header>
                <div class="logo">
                    <h2>
                        <a href="<?php echo home_url();?>" title="<?php _e('Go to homepage', 'plainwp');?>">
                            <?php bloginfo('name');?>
                        </a>
                    </h2>
                </div>
                <nav>
                    <?php plainwp_mainmenu();?>
                </nav>
            </header>
