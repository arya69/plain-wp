<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */
get_header();
?>
<div class="single-post">
    <?php if (have_posts()):while (have_posts()):the_post();?>
        <?php if(has_post_thumbnail()){
            the_post_thumbnail();
        }?>
            <h1>
                <?php the_title();?>
            </h1>
            <div class="meta">
                <span class="tag"><?php the_tags(__('Tags: ','plainwp'),', ','');?></span><br/>
                <span class="author"><?php _e( 'Published by', 'plainwp' ); ?> <?php the_author_posts_link(); ?> at <?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span><br/>
                <span class="categories"><?php _e( 'Categorised in: ', 'plainwp' ); the_category(', ');?></span>
            </div>
            <?php 
            the_content();
            $prev = get_previous_post(); 
            $next = get_next_post();
            ?>
            <?php if($prev){?>
            <a href="<?php echo get_permalink($prev->ID); ?>" title="<?php echo esc_attr($prev->post_title); ?>"><- Prev</a>
            <?php }
            if($next){?>
            <a href="<?php echo get_permalink($next->ID); ?>" title="<?php echo esc_attr($next->post_title); ?>">Next -></a>
            <?php }
            edit_post_link();
            comments_template();
            ?>
    <?php endwhile;else:?>
        <div class="no-post">
            <h2><?php _e('Sorry, no post here.', 'plainwp');?></h2>
        </div>
    <?php endif;?>
</div>
<?php
get_sidebar();
get_footer();
?>