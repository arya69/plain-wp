<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */

//remove admin bar
add_filter( 'show_admin_bar', '__return_false' );

//welcome text
//register setting to display pointer once
add_action('admin_init', 'register_pointer_setting');
function register_pointer_setting(){
    register_setting('plainwpadmin', 'plainwp_pointer');
}
if (get_option('pointer') == 'false'){
    add_action( 'admin_enqueue_scripts', 'my_admin_enqueue_scripts' );
    function my_admin_enqueue_scripts() {
        wp_enqueue_style( 'wp-pointer' );
        wp_enqueue_script( 'wp-pointer' );
        add_action( 'admin_print_footer_scripts', 'my_admin_print_footer_scripts' );
    }
    function my_admin_print_footer_scripts() {
        $pointer_content = '<h3>Welcome | PlainWP</h3>';
        $pointer_content .= '<p>Thanks for installing PlainWP theme, you can start setting your theme here.</p>';
    ?>
       <script type="text/javascript">
       //<![CDATA[
       jQuery(document).ready( function($) {
        $('#toplevel_page_redux_options').pointer({
            content: '<?php echo $pointer_content; ?>',
            position: 'left',
            close: function() {
                // Once the close button is hit
                //update option pointer
                var data = {
                    action: 'plainwp_action',
                    function: 'update_pointer'
                };
                $.post(ajaxurl, data, function(response){
                    if(response){
                        var _data = JSON.parse(response);
                        console.log(_data);
                    }
                });
            }
          }).pointer('open');
       });
       //]]>
       </script>
    <?php
    }
    add_action('wp_ajax_plainwp_action', 'plainwp_action_callback');
    function plainwp_action_callback(){
        if($_POST['function'] == 'update_pointer'){
            update_option('plainwp_pointer', 'true');
        }
    }
}

//enable html5 shim for IE
function plainwp_IEhtml5_shim(){
    if(!is_admin()){
        global $is_IE;
        if($is_IE)
            echo '<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->';
    }
}
add_action('wp_head','plainwp_IEhtml5_shim');

//allow usert login using username or email address
function login_email($username){
    $user = get_user_by('email', $username);
    if(!empty($user->user_login))
        $username = $user->user_login;
    return $username;
}
add_action('wp_authenticate','login_email');

//change username text
function change_user_text($text){
    if ($text == 'Username'){
        $text = 'Username Or Email';
    }return $text;
}
add_filter( 'gettext', 'change_user_text' );

//change failed login message text
function failed_login(){
    return '<strong>ERROR : </strong>the login information you have entered is incorrect.';
}
add_filter ( 'login_errors', 'failed_login' );

//add custom classes if menu has a child
function menu_set_dropdown($sorted_menu_items, $args){
    $last_top = 0;
    foreach ($sorted_menu_items as $key => $obj){
        if (0 == $obj->menu_item_parent){
            $last_top = $key;
        }else{
            $sorted_menu_items[$last_top]->classes['dropdown'] = 'has-dropdown';
        }
    }
    return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'menu_set_dropdown', 10, 2 );

function roots_wp_nav_menu($text) {
  $replace = array(
    'sub-menu'=>'dropdown',
    'current-menu-item'     => 'active',
    'current-menu-parent'   => 'active',
    'menu-item-type-post_type' => '',
    'menu-item-object-page' => '',
  );

  $text = str_replace(array_keys($replace), $replace, $text);
  return $text;
}
add_filter('wp_nav_menu', 'roots_wp_nav_menu');

//Redirect To Post If Search Results Return One Post
function redirect_single_post() {
    if (is_search()) {
        global $wp_query;
        if ($wp_query->post_count == 1) {
            wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );
        }
    }
}
add_action('template_redirect', 'redirect_single_post');

//highlight search result
function wps_highlight_results($text){
     if(is_search()){
     $sr = get_query_var('s');
     $keys = explode(" ",$sr);
     $text = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">'.$sr.'</strong>', $text);
     }
     return $text;
}
add_filter('the_excerpt', 'wps_highlight_results');
add_filter('the_title', 'wps_highlight_results');

//Remove unneeded images / thumbnail sizes

update_option( 'thumbnail_size_h', 0 );
update_option( 'thumbnail_size_w', 0 );
update_option( 'medium_size_h', 0 );
update_option( 'medium_size_w', 0 );
update_option( 'large_size_h', 0 );
update_option( 'large_size_w', 0 );

/*=====================
 * custom login logo
 * ==================== */
function custom_login_logo(){
//    echo '<style type="text/css">
//            h1 a { background-image:url("logo url") !important; }
//        </style>';
}
add_action('login_head', 'custom_login_logo');

/*=======================
 * add css to admin head
 * ====================== */
function admin_css(){
    echo '<link rel="stylesheet" type="text/css" href="'.  get_template_directory_uri().'/css/admin.css" />';
//    echo '
//        <style>
//            body.login{background-color: '.$opts['login_color'].' !important;}
//        </style>
//    ';
}
add_action('admin_head', 'admin_css');

/*=====================
 * Comments
 * ==================== */

//remove url field, enable this function if you want to remove url field
//add_filter('comment_form_default_fields', 'unset_url_field');
//function unset_url_field($fields){
//    if(isset($fields['url']))
//       unset($fields['url']);
//       return $fields;
//}

// Custom Comments Callback
function wpplaincomment($comment, $args, $depth){
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    if ('div' == $args['style']){
        $tag = 'div';
        $add_below = 'comment';
    }else{
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
        <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
            <?php if ( 'div' != $args['style'] ) : ?>
            <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
            <?php endif; ?>
            <div class="comment-author vcard">
            <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $size = '100' ); ?>
            <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
            </div>
    <?php if ($comment->comment_approved == '0') : ?>
            <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
            <br />
    <?php endif; ?>

            <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                    <?php
                            /* translators: 1: date, 2: time */
                            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
                    ?>
            </div>

            <?php comment_text() ?>

            <div class="reply">
            <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>
            <?php if ( 'div' != $args['style'] ) : ?>
            </div>
        <?php endif; ?>
    <?php
}

//force perfect JPG images
function plainwp_jpeg_quality(){
    return 100;
}
add_filter('jpeg_quality', 'plainwp_jpeg_quality');

/*=================
 * Plugins
 * ================ */

/******************************************************************************
 * contact form 7
 * deregister contact form 7 script & style enable if you using contact form 7
 * **************************************************************************** */

//add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 );
//function deregister_cf7_javascript() {
//    if ( !is_page(array(8,10)) ) {
//        wp_deregister_script( 'contact-form-7' );
//    }
//}
//add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 );
//function deregister_cf7_styles() {
//    if ( !is_page(array(8,10)) ) {
//        wp_deregister_style( 'contact-form-7' );
//    }
//}

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
?>
