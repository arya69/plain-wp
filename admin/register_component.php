<?php
/*  ============================
 *  Author : Adi Ardana
 *  ============================ */
/* ================================
 * 
 * register menu
 *
 * ===============================*/

function register_plainwp_menu(){
    register_nav_menus(array(
        'main-menu'     => __('Main Menu', 'plainwp'),
        'extra-menu'    => __('Extra Menu', 'plainwp')
    ));
}add_action('init','register_plainwp_menu');

function plainwp_mainmenu(){
    wp_nav_menu(
        array(
            'theme_location'  => 'main-menu',
            'menu'            => '', 
            'container'       => '', 
            'container_class' => '', 
            'container_id'    => '',
            'menu_class'      => 'menu', 
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul>%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        )
    );
}
function plainwp_extramenu(){
    wp_nav_menu(
        array(
            'theme_location'  => 'extra-menu',
            'menu'            => '', 
            'container'       => 'div', 
            'container_class' => '', 
            'container_id'    => '',
            'menu_class'      => 'menu', 
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '',
            'depth'           => 0,
            'walker'          => ''
        )
    );
}

/* ================================
 * 
 * register sidebar
 *
 * ===============================*/
if (function_exists('register_sidebar')){
    register_sidebar(array(
        'name'          => __('1st Sidebar', 'plainwp'),
        'id'            => 'sidebar1',
        'description'   => __('First Sidebar', 'plainwp'),
        'class'         => '',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   =>''
    ),array(
        'name'          => __('2nd Sidebar','plainwp'),
        'id'            => 'sidebar2',
        'description'   => __('Second Sidebar','plainwp'),
        'class'         => '',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ));
}
?>