<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
  *  ============================ */
get_header();
?>
<div class="<?php echo $post->post_name;?>-page">
    <h1><?php the_title();?></h1>
    <?php if(have_posts()):while (have_posts()):the_post();?>
    <div class="post">
        <?php 
        the_content();
        comments_template('', true);
        echo '<br/>';
        edit_post_link();
        ?>
    </div>
    <?php endwhile;else:?>
        <div class="no-post">
            <h2><?php _e('Sorry, no post here.', 'plainwp');?></h2>
        </div>
    <?php endif;?>
</div>
<?php
get_sidebar();
get_footer();
?>