<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */

get_header();
?>
<div class="error-404-page">
    <h1>404</h1>
    <h2><?php _e('Page not found','plainwp');?></h2>
    <?php get_template_part('searchform');?>
</div>
<?php
get_sidebar();
get_footer();
?>