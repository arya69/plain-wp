<?php

/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */
?>
<aside id="sidebar">
    <div class="sidebar-widget">
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar1')) ?>
    </div>
    <div class="sidebar-widget">
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar2')) ?>
    </div>
</aside>