<?php
/*  ============================
 *  Author : Adi Ardana
 *  Dewata Production
 *  ============================ */

get_header();
?>
<div class="archive-page">
    <h1><?php _e('Archives','plainwp');?></h1>
    <?php get_template_part('loop');?>
    
    <div class="pagination">
        <?php plainwp_pagination();?>
    </div>
</div>
<?php
get_sidebar();
get_footer();
?>